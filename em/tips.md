

Tips for new EMs
-

This transition is big. Life of an EM is very different than life of an IC. It's not better/worse, it's different.

Changes you will notice immediately:

 - more meetings
 - more preparing for meetings
 - more events in weekly cycles
 - a little bit less time flexibility due to the number of meetings

 Changes you will notice in the long run:

 - bigger sphere of influence
 - being exposed to problems and plans that will be worked on not weeks, but months from now
 - more connecting dots
 - no time for coding :D

Great tips I have gathered:
- Once you have too much on your plate, ask yourself: if I am the only one who can do it? If yes, this stays on your plate (think hiring, performance reviews etc). If not - you can delegate.
- If you have time for IC work, you are probably missing some EM work ;P.


Sources I have found particularly interesting:
- [this](https://gitlab.com/gitlab-com/book-clubs/-/issues/28) is such an easy and pleasant read and it has everything you need to know at the beginning.
- MENTORING. Imho this is so valuable and changes everything. Having access to someone very experienced to ask questions, confront ideas and being able to ask questions outside of your leadership ladder is priceless. I genuinely think that without mentoring I wouldn't be able to get where I am.
-  venting buddy. This transition is hard sometimes and can be stressful (I hope it won't be for you, maybe I am prone to stress) and having venting buddy - someone in the similar role to talk to regularly - was life-saver more than once.
