## lifelabs coaching - 2022-09-03

small changes: big impact

my challenges: 
- too many things to focus / prioritize
- team still forming


Question skills: question quantity vs quality

great manager with direct report: 10 questions in 15 minutes :) 

- can you tell more
- how do you think it should be handled
- what do you want to achieve
- what are your thoughts on it so far? 

Why questions? Becouse otherwise managers are solving the wrong thing

-> make sure you understand the problem, what point you're trying to make

 **be rubber duck for our teams**

Empower them to take ownership

q-step: always question before advice

QUESTION MODE FIRST AND DEFAULT

> example: someone wants to start new project
> WHY?
> what are the other projects you're working on? 

Authentic questions, questions agility

stuck? => pivot

#### 3 coaching tools
1a) playback - just to make sure I understand... / you said... / Did I get this right? 
1b) split tracks - I hear a few things: x and y. Which one should we focus on first? 

train people to think clearly :) 



Reference:  https://gitlab.com/gitlab-com/people-group/learning-development/training-curriculum/-/issues/56
