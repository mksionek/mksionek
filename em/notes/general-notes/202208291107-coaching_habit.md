# coaching habit - 2022-08-29

how coaching can help you? 
by breaking 3 vicious cycles: 
- overdepandence dance
	- help your team become more self-sufficient and reduce your need to jump in
- overwhelmed, lack of focus, more overwhelmed
	- by coaching - team does work that has real impact
- disconnected 
	- coaching helps team do work that matters

Seven questions => heart of the book

##### how to build a habit? 
45% of our waking behaviour is habitual 
build a habit: 
- reason
- trigger
- a microhabit
- effective practice
- a plan

think less about what habit can do for you, more what can do for the people around you 

what triggers old behaviours? 

micro-habit: less than 60 seconds. short and spefic
deep practice: practice small chunks, repeat, celebrate success 

resilient systems: build in fail-safe, so when something breaks it is easy to recover and the next step is obvious
HABITS NEED TO BE RESILIENT SYSTEM.

- identify trigger
- identify old habit
- define new behaviour

- start somewhere easy
- start small
- buddy up 

JUST ONE QUESTION AT THE TIME: avoid the interrogation effect
Habit: after I ask question I will wait quietly for the answer


#### the kickstart question
problems: small talk/ossified agenda/default diagnosis

_What is on your mind?_ what is the most important thing? 

coaching for performance: issue-focused
coaching for development: person-focused

3P model: what's a challange in this situation: 
Project? 
Person? 
Pattern of behaviour? 

trigger: any conversation: new habit: ask "what's in your mind"

We are what we pay attention to. 
If you're afraid of asking a question and you're doing a build-up: cut it. Just ask it. 

#### the AWE question

AND WHAT ELSE?

Decisions that are made form binary choices fail at 50% rate
having more choices gave failure rate of 30%

Tame advice monster! it also gives you more time. 

-> stay curious, stay genuine - deep practice that. 

be aware of decision making paralysis

! do not disguise your ideas as questions! 


##### the FOCUS question

a need to "fix the issue" to get things done? 
- sometimes causing to fix the wrong problem
- solving problems yourself - team should fix it
- not solving problem at all

Q: WHAT IS THE REAL CHALLENGE HERE FOR YOU? 

foggy things: 
- too many challenges ("if you had to pick one of those to focus - which one would be the real challenge for you?") 
- gossiping and moaning: where is the real challenge _for you_?
- abstractions and generalizations: go back to the person

FOR YOU part makes it more about the development than performance

1) trust that you are useful 
2) remember to add "AND WHAT ELSE"
3)

#### the foundation question
WHAT DO YOU REALLY WANT? 
distinction between _needs_ and _wants_
according to Rosenberg, needs are under wants

Safe brain: 
- more sophisticated level 
- more subtle thinking 
- better able to see and manage ambiguity 
- assuming positive intent

unsafe brain: 
- amygdala hijack
- backing away
- THIS IS DEFAULT BEHAVIOUR


TERA model - how brain reacts to the situation
- tribe - with me or against me
- expectations - do I know the future, is what happens next clear
- rank - are you more or less important
- autonomy - do I have a say? 

Jobs: increase TERA quotient
this question does that: 
Miracle questions (from solutions-based therapy): Miracle happens, how would you know things are better? 

-> Staying silent after question <-


Avoid being helpful => stepping in and taking over, lowering someone else's status

Karpman: we're jumping between 3 roles
- victim - no responsibility for fixing, powerlessness
- persecutor: other's less good than me
- rescuer: fixing, believe you're indispensible, problem solving mode

How can I help? - lazy question - other need to do direct a clear request. 
DO NOT assume you know how to help

Saying no to the questions is an option. You can also say "I need to think about it" or propose a counter-offer. 

If someone asks "what should I do":
- that's a great question
- I've got some ideas
- but what are your first thoughts


when asking questions: listen to the answers!!! 

--

Good work: day to day
Great work: more meaningful and impactful 

Busy is not good, is lazy, if we're only doing good work

Strategic question: if you're saying yes to this, what are you saying no to? 

Say yes slowly, ask more questions

5 strategic questions: 
- what is our winning aspiration
- where will we play? 
- how will we win? 
- what capabilities must be in place? 
- what management styles are required? 


Acknowledge the answer before jumping into the next question

LEARNING QUESTION:
learning when they do - recall-reflect
create space for people to have learning moments

WHAT WAS THE MOST USEFUL FOR YOU? 

##### AGES model: 
Attention
Generation (ask and answer questions yourself)
Emotion
Spacing

Interrupt forgetting, for example by asking questions at the end of the conversation 
Ask people what they have learned, what is important to capture
Why?
- assumes conversation was useful
- identyfing big things that was the most useful 
- makes it personal
- gives you feedback
- it's learning, not a judgement




Reference: https://www.goodreads.com/book/show/29342515-the-coaching-habit
