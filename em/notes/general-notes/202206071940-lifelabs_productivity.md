# productivity and prioritization - 2022-06-07
there are countless productivity tools, what came up frequently? 

4 challenges across companies: 
- chronemics - time awareness
	- how long something can take
	- how long something was taking in the past
- prioritization: what gives the greatest value
- ogranization: efficient workflow
- focus: working without disruptions

Chronemics: 
cultivate time integrity: no being late, finishing on time
clean up blur words like two seconds, asap, sometime etc
use time language (for example, 10 minutes before the end of the meeting: "in our last 10 minutes")
be aware of planned time vs reactive time

Prioritization: MIT method: most important things
eisenhower matrix: 
|  | Urgent | Not urgent |
|-|---------| ---------- |
|Important| FIRES Q1 |INVESTMENT Q2|
|Not Important|DECEPTION Q3|RECHARGE Q4|

Q2: calendar-block to focus on this
Q3: Is it the MIT? 
Am I the best one to do it or can I delegate?
Is it already good enough? 
Here are things that do not matter, pseudo-progress, emails, quick questions
Q4: Is it urgent? Is it Important?
We can count breaks (or micro-breaks here)

Bucket method: 
Decide which 3 things are the most important for this quarter + other bucket
categorize tasks for this weeks into those buckets
reduces overwhelm, help with bucket trade-offs


Open loops take brain space (Zeigarnik effect)
Consistent capture system (to-do lists/appointments/notes) helps closing the loop
closed loop culture: Who will own this? Who is capturing this? how we should follow up on this? 

Focus: 44% of interuptions are self-interuptions
context switch causes cognitive lag

What helps: pomodoro technique, If-then cues (if it is morning, I will check my email etc)

Reference: https://gitlab.com/gitlab-com/people-group/learning-development/training-curriculum/-/issues/56
