# Resilient Management - 2022-05-29

- team mates: direct reports
- feature team (different disciplines) - functional teams - same discipline

#### Meet your team
do not assume your style is everybody else's style
needs different approaches
Brain: 
- prefrontal cortex: rationality, logic
- amygdala: fight or flight response

Core needs: ones that we need to fulfill to feel secure

6 core needs in the workplace: 
- Belonging (because Amygdala) - sense of belonging used to be our thing for 
- improvement/progress - people get demotivated but not impactful tasks
- choice - too many/too few: bad
- equality, fairness - lack of it is bad for organization
- predictability- yes, but no changes make brains bored
- significance - where are we in the hierarchy and if we're contributing to something valuable

BICEPS needs
not every need is equally important for everyone
Same stimulus can threaten different needs for different people. 

Gather information about feedback and recognition preferences.
Use "first 1:1" questions. - link

Help the team to get to know you
What you are optimizing for

Share some of your answers to 1:1 questions

#### Grow your teammates
Managers are there to navigate storms by helping the team grow into their roles

1) mentoring: lending advice based on own experience
	1) some advice can land better with one person than another
	2) check in if you're right 
	3) be careful with urg
2) coaching: asking open questions to help reflect and introspect
	1) Reflecting: telling them what you see and hear to tell them reflect
	2) avoid: 
		1) how questions: problem solving mode
		2) why questions: judgy 
		3) ask from the place of genuine curiosity: help people feel, be seen and heard
			1) actual curiosity, not judgy
		4) "What I'm hearing it's... is that right"
		5) holding metaphorical mirror
		6) help your teammates develop their own brain wrinkles :)
3) sponsoring: finding good opportunities to level up and get promoted
	1) adding someones name for opportunities
	2) help get visibility and good assignments
	3) assigning stretch tasks just beyond their skill set
	4) urg members are under-sponsored but overmentored
4) deliver feedback
	1) Regularly delivered
	2) clear picture what is working
	3) specific
	4) actionable
	5) delivered in a way that is possible to absorb
	6) job description as a central point
	7) actionable feedback = observation of behaviour + impact of behaviour + request/question
		1) describe simple facts
		2) sharing the impact beyond surface level feelings, focus on the effect
		3) request - but better a question
		4) good sponsorship opportunities after the feedback
Delivering feedback about the behaviour that was not witnessed directly: harder. Focus on the aspect that you can personally own. 

coach others on delivering feedback! 

1:1 goals: 
- build trust
- gain shared context
- plan out and support career growth
- solve problems
coaching and open questions are better for building trust and helping someone grow

balance those activities

do not pass problematic team mate to another manager!!


Document how team shoudl collaborate and communicate, document team's vision
this would fulfill PREDICTABILITY, BELONGING, EQUITY/FAIRNESS needs

Roles and responsibilities: job description and project specific expectations

Rescponsibility assignment matrix: RACI matrix
- responsible: who do the work
- accountable: who ensures things are done timely and quality is there and communicate to the stakeholders
- consulted
- informed

Responsibility Venn diagram: Who does what and what is shared - helpful when multiple people with different roles work toward common goal

TEAM VISION AND PRIORITIES
- north star
- OKR/KPI

Vision => Mission => Objectives => Strategy
Each one more granular
Strategy is measurable goals

Document team practices 
- meetings (description, time, who should come, goals)
- email group
- slack channels

Empower all to help shape how the team works together
team holding each other accountable
Regularly ask for feedback for the processes and for documentation
Setting expectations will build solid foundations for team's growth

#### Communication
strategic information: communication plan 
- the what
- the why
- who knows
- who will be impacted
- what will be said where and when
- talking points

when communicating - think of which BICEPS needs can be threaten, what questions can be asked, how will you help them get onboard. 

- map big changes back to the things people care about
- choose planned words carefully
- plan out who can be informed when 
- optimize for creating clarity and transparency
- remember that reactions of others can threaten your amygdala - engage your prefrontal cortex

when you disagree and can't commit - be transparent and professional about it 
ask questions, give feedback, share the potential negative things

disagree and commit - put reservations on hold OR leave. 

Meetings are not bad for communicating
recap emails after the meeting
remember about context
Decide which energy this communication requires (color-communication classification)

***listen what people care about and optimize for and craft your message accordingly***

#### Resiliency
change will happen
amygdala hijacked: hard to process new information 

someone going through something: 
	- Describe clearly expectations
	- know your company benefits well
	- lead by example
	- ask for input
	- keep setting expectations - clarity

ASK: if they anticipate a dip in productivity, consider asking them what we can do to meet this goal? 

do not require them to reassure you 
remind people of available resources when something is going on 
be careful about your own energy levels - you can color-code your calendar with the types of things you're doing - and which brains are you using
be aware of what kind of energy certain meetings require

if you delegate hard project to a team - don't make it all easy on them
- tell them how will you support them
- tell them you know it will be hard
- Use a RACI matrix - you are consulted and accountable

Stay in coaching mode to help others grow


Build a support network, focus on people with skills different than yours, 
make it easy for people to say yes, be specific with your ask

Strong connection with your peers is your number one priority





Reference: 
https://resilient-management.com/

