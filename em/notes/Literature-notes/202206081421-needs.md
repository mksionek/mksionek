# needs - 2022-06-08
#### Meet your team
do not assume your style is everybody else's style
needs different approaches
Brain: 
- prefrontal cortex: rationality, logic
- amygdala: fight or flight response

Core needs: ones that we need to fulfill to feel secure

6 core needs in the workplace: 
- Belonging (because Amygdala) - sense of belonging used to be our thing for 
- improvement/progress - people get demotivated but not impactful tasks
- choice - too many/too few: bad
- equality, fairness - lack of it is bad for organization
- predictability- yes, but no changes make brains bored
- significance - where are we in the hierarchy and if we're contributing to something valuable

BICEPS needs
not every need is equally important for everyone
Same stimulus can threaten different needs for different people. 

Gather information about feedback and recognition preferences.
Use "first 1:1" questions. - link

Help the team to get to know you
What you are optimizing for

Share some of your answers to 1:1 questions

Reference: [[202205291346-resilient-management]]
