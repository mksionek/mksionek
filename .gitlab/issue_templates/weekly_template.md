**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [ ]
- [ ]
- [ ]

**Every week tasks:**

***Monday:***
- [ ] Alex 1:1 prep
- [ ] Check recruitment
- [ ] Check error budget
- [ ] triaging
- [ ] OKRs update
- [ ] Async weekly

***Tuesday:***

- [ ] Engineering allocation meeting prep
- [ ] Abdul 1:1 prep
- [ ] Serena 1:1 prep
- [ ] Manoj 1:1 prep
- [ ] Peter 1:1 prep
- [ ] Hitesh 1:1 prep
- [ ] Christina 1:1 prep
- [ ] Mansoor 1:1 prep

***Wednesday:***

- [ ] Michelle 1:1 prep

***Thursday:***
- [ ] Focus work

***Friday:***
- [ ] Focus work

**If time permits:**
- [ ]

**This week learnings:**
-
