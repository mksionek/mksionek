<details>

<summary> <b> Template </b> </summary>
*DATE*

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [ ] IMOC shift (Friday-Sunday)
- [ ] Performance review (MK)
- [ ] Performance review (HR)
- [ ] Performance review (AP)

**Every week tasks:**

***Monday:***
- [ ] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [ ] OKRs update
- [x] Async weekly

***Tuesday:***

- [ ] Engineering allocation meeting prep
- [ ] Abdul 1:1 prep
- [ ] Serena 1:1 prep
- [ ] Manoj 1:1 prep
- [ ] Peter 1:1 prep
- [ ] Hitesh 1:1 prep
- [ ] Christina 1:1 prep
- [ ] Mansoor 1:1 prep

***Wednesday:***

- [ ] Michelle 1:1 prep

***Thursday:***
- [ ] Focus work

***Friday:***
- [ ] Focus work

</details>

# 2022-11-14

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [ ] IMOC shift (Friday-Sunday)
- [ ] Performance review (MK)
- [ ] Performance review (HR)
- [ ] Performance review (AP)

**Every week tasks:**

***Monday:***
- [ ] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [ ] OKRs update
- [x] Async weekly

***Tuesday:***

- [ ] Engineering allocation meeting prep
- [ ] Abdul 1:1 prep
- [ ] Serena 1:1 prep
- [ ] Manoj 1:1 prep
- [ ] Peter 1:1 prep
- [ ] Hitesh 1:1 prep
- [ ] Christina 1:1 prep
- [ ] Mansoor 1:1 prep

***Wednesday:***

- [ ] Michelle 1:1 prep

***Thursday:***
- [ ] Focus work

***Friday:***
- [ ] Focus work


<details>
<summary> <b> Older entries </b> </summary>

# 2022-11-07

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :ferris_wheel:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [ ] OKRs
- [ ] Planning
- [ ] performance review

**Every week tasks:**

***Monday:***
- [x] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [x] OKRs update
- [x] Async weekly

***Tuesday:***

- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [x] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Christina 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [x] Focus work

**If time permits:**
- [ ]


# 2022-10-31

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] catching up after PTO
- [x] Workday assessments
- [x] OKRs

**Every week tasks:**

***Monday:***
- [x] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [ ] OKRs update
- [x] Async weekly

***Tuesday:***

- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [ ] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Christina 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [x] Focus work

***Friday:***
- [ ] bugs squashing check
- [x] Focus work

**If time permits:**
- [ ]

## 2022-03-10

**My availability:**

Monday - :palm_tree:/:computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :sunny:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] calibrations
- [x] prepare for planning
- [x] IAmRemarkable workshops

**Every week tasks:**

***Tuesday:***

- [x] Async weekly
- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [x] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Christina 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [x] Focus work

***Friday:***
- [ ] bugs squashing check
- [ ] Check recruitment
- [ ] Check error budget
- [x] triaging
- [ ] OKRs update
- [ ] prepare myself for career development discussion

**If time permits:**
- [ ]

## 2022-09-26

**My availability:**

Monday - :computer:/:sunny: (half day working)

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :palm_tree:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [ ] more work on performance reviews
- [ ] finish dev on call planning

**Every week tasks:**

***Monday:***
- [x] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [x] OKRs update
- [x] Async weekly

***Tuesday:***

- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [x] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Christina 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [ ] Focus work

***Friday:***
- [ ] Focus work
- [ ] bugs squashing

## 19.09.2022

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] performance reviews
- [x] IMOC SHIFT for 8 days
- [ ] prepare rails girls presentation

**Every week tasks:**

***Monday:***
- [x] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [x] OKRs update
- [x] Async weekly

***Tuesday:***

- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [x] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Christina 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [x] Focus work

***Friday:***
- [x] Focus work

**If time permits:**
- [ ]

## 12.09.2022

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] planning
- [x] api discussions
- [x] dev on call
- [x] slowly start with performance reviews

**Every week tasks:**

***Monday:***
- [x] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [x] OKRs update
- [x] Async weekly

***Tuesday:***

- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [x] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Christina 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [x] Focus work

***Friday:***
- [x] Focus work


### 05.09.2022

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [ ] backlog
- [x] codeowners
- [x] 360 feedback
- [ ] API problem

**Every week tasks:**

***Monday:***
- [x] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [x] OKRs update
- [x] Async weekly

***Tuesday:***

- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [x] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Christina 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [x] Focus work

***Friday:***
- [x] Focus work

### 30.08.2022

**My availability:**

Monday - :sunny:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] Break down of two issues from Christina
- [x] Promo doc
- [x] backlog
- [x] script for counting closed bugs
- [x] api problem

**Every week tasks:**

***Tuesday:***

- [x] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [x] OKRs update
- [x] Async weekly
- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [x] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [x] Focus work

***Friday:***
- [x] Focus work

<details>